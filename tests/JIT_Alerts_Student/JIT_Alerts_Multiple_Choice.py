import time

import pytest
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

from config.BaseClass import BaseClass
from data import TD_JIT_Alerts_Student
from data.TD_JIT_Hint_Teacher import TD_JIT_Hint_Teacher

from pages.HomePage import HomePage
from pages.LearningPage import LearningPage
from pages.Learning_Template_Outline_ComponentPage import Learning_Template_Outline_ComponentPage
from pages.Learning_Template_Outline_JIT_AlertsPage import Learning_Template_Outline_JIT_AlertsPage
from pages.Learning_Template_Outline_JITcomponentPage_2nd import Learning_Template_Outline_JITcomponentPage_2nd

from pages.LoginPage import LoginPage


# Pre condition :
# Student data should be minimum  2 page and one page with one component
# and no answer was submit before

class Test_Learning_Template_JIT_Alerts_Student(BaseClass):

    @pytest.mark.parametrize('select_lession',
                             [
                                 (TD_JIT_Alerts_Student.S_JITAlerts_Lesson_multipleChoice)

                             ]
                             )
    def FirstandSecont_tattempt_validateOnContinueBtn_JITAlerts_Student_lesson(self, select_lession):

        try:
            log = self.getLogger()
            loginpage = LoginPage(self.driver)
            homepage = HomePage(self.driver)
            learningpage = LearningPage(self.driver)
            learningtemplatecomponentpage = Learning_Template_Outline_ComponentPage(self.driver)
            learningtemplateoutlineJITcomponentpage2nd = Learning_Template_Outline_JITcomponentPage_2nd(self.driver)
            Learning_Template_Outline_JIT_Alertspage = Learning_Template_Outline_JIT_AlertsPage(self.driver)
            loginpage.enter_username(TD_JIT_Alerts_Student.UseName_Student)
            loginpage.enter_password(TD_JIT_Alerts_Student.Password_Student)
            loginpage.clk_login()
            time.sleep(10)
            log.info("Login Successfully Completed")
            homepage.clk_learningLink()
            time.sleep(10)
            learningpage.clk_ActiveCoursesTab()
            learningpage.SearchActiveCourseSearchbox(TD_JIT_Alerts_Student.S_JITAlerts_ActiveCourse)
            time.sleep(5)
            learningtemplatecomponentpage.clk_choose_Templates()
            time.sleep(20)
            Learning_Template_Outline_JIT_Alertspage.clk_Progress()
            learningtemplatecomponentpage.chooseunit(TD_JIT_Alerts_Student.S_JITAlerts_Unit)
            learningtemplatecomponentpage.Choose_topic(TD_JIT_Alerts_Student.S_JITAlerts_Topic)
            learningtemplatecomponentpage.chooseday(TD_JIT_Alerts_Student.S_JITAlerts_Day)
            time.sleep(40)
            learningtemplatecomponentpage.chooselesson(select_lession)
            time.sleep(20)

            # 1st click on continue , JIT Alert validate and Page has not been navigated to 2nd page
            Learning_Template_Outline_JIT_Alertspage.clk_continue()
            # Learning_Template_Outline_JIT_Alertspage.First_click_on_continue_and_Validate_JIT_Alert_massage()
            ele_alert_text = self.driver.find_element_by_xpath(
                "//div[@class='Toastify__toast-container Toastify__toast-container--bottom-right jit-alert-wrapper']//div[@class='jit-alert']").text
            log.info(ele_alert_text)
            # assert("Hey! It looks like you missed a question. Give it a try.",ele_alert_text)
            if ("Hey!" and "It looks like you missed a question." and "Give it a try.") in ele_alert_text:
                log.info("First click on continue on " + select_lession + " and Validate JIT Alert massage .")
            else:
                log.info("First click on continue on " + select_lession + " and Not Validated JIT Alert massage .")

            # 2nd click on continue , Page has been navigated to 2nd page
            time.sleep(10)
            Learning_Template_Outline_JIT_Alertspage.clk_continue()
            Learning_Template_Outline_JIT_Alertspage.Second_click_on_continue_Page_has_been_navigated_to_2nd_page()

            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed"}}')
        except:

            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed"}}')

    @pytest.mark.parametrize('select_lession1', TD_JIT_Alerts_Student.S_JITAlerts_Lesson_multipleChoice)
    def FirstandSecond_tattempt_validateOnPagination_JITAlerts_Student_lesson(self, select_lession1):
        try:
            log = self.getLogger()
            loginpage = LoginPage(self.driver)
            homepage = HomePage(self.driver)
            learningpage = LearningPage(self.driver)
            learningtemplatecomponentpage = Learning_Template_Outline_ComponentPage(self.driver)
            learningtemplateoutlineJITcomponentpage2nd = Learning_Template_Outline_JITcomponentPage_2nd(self.driver)
            Learning_Template_Outline_JIT_Alertspage = Learning_Template_Outline_JIT_AlertsPage(self.driver)
            loginpage.enter_username(TD_JIT_Alerts_Student.UseName_Student)
            loginpage.enter_password(TD_JIT_Alerts_Student.Password_Student)
            loginpage.clk_login()
            time.sleep(10)
            log.info("Login Successfully Completed")
            homepage.clk_learningLink()
            time.sleep(10)
            learningpage.clk_ActiveCoursesTab()
            learningpage.SearchActiveCourseSearchbox(TD_JIT_Alerts_Student.S_JITAlerts_ActiveCourse)
            time.sleep(5)
            learningtemplatecomponentpage.clk_choose_Templates()
            time.sleep(20)
            Learning_Template_Outline_JIT_Alertspage.clk_Progress()
            learningtemplatecomponentpage.chooseunit(TD_JIT_Alerts_Student.S_JITAlerts_Unit)
            learningtemplatecomponentpage.Choose_topic(TD_JIT_Alerts_Student.S_JITAlerts_Topic)
            learningtemplatecomponentpage.chooseday(TD_JIT_Alerts_Student.S_JITAlerts_Day)
            time.sleep(40)
            learningtemplatecomponentpage.chooselesson(select_lession1)
            time.sleep(20)

            pape2nd = self.driver.find_element_by_xpath("//li[@class='c-editor-pagebreak__nav__pages-item'][2]/a")
            # pagination 1st click
            pape2nd.click()
            ele_alert_text = self.driver.find_element_by_xpath(
                "//div[@class='Toastify__toast-container Toastify__toast-container--bottom-right jit-alert-wrapper']//div[@class='jit-alert']").text
            log.info(ele_alert_text)
            # assert("Hey! It looks like you missed a question. Give it a try.",ele_alert_text)
            if "Hey!" in ele_alert_text:
                if "It looks like you missed a question." in ele_alert_text:
                    if "Give it a try." in ele_alert_text:
                        log.info("First click on Page2 on " + select_lession1 + "and Validate JIT Alert massage .")
            else:
                log.info("First click on Page2 on " + select_lession1 + " and Not Validated JIT Alert massage .")

            time.sleep(10)
            # pagination 2nd click
            pape2nd.click()
            pagination = self.driver.find_element_by_xpath(
                "//li[@class='c-editor-pagebreak__nav__pages-item'][2]/a")
            paginationText2nd = pagination.get_attribute("title")
            log.info(paginationText2nd)
            if "Page 2" in paginationText2nd:
                assert True
                log.info("Second click on Page2 and page has been navigated to 2nd page.")

            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed"}}')
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed"}}')

    # def FirstandSecond_tattempt_validateOnPagination_JITAlerts_Student_lesson(self, select_lession1):
    #     try:
    #         log = self.getLogger()
    #         loginpage = LoginPage(self.driver)
    #         homepage = HomePage(self.driver)
    #         learningpage = LearningPage(self.driver)
    #         learningtemplatecomponentpage = Learning_Template_Outline_ComponentPage(self.driver)
    #         learningtemplateoutlineJITcomponentpage2nd = Learning_Template_Outline_JITcomponentPage_2nd(self.driver)
    #         Learning_Template_Outline_JIT_Alertspage = Learning_Template_Outline_JIT_AlertsPage(self.driver)
    #         loginpage.enter_username(TD_JIT_Alerts_Student.UseName_Student)
    #         loginpage.enter_password(TD_JIT_Alerts_Student.Password_Student)
    #         loginpage.clk_login()
    #         time.sleep(10)
    #         log.info("Login Successfully Completed")
    #         homepage.clk_learningLink()
    #         time.sleep(10)
    #         learningpage.clk_ActiveCoursesTab()
    #         learningpage.SearchActiveCourseSearchbox(TD_JIT_Alerts_Student.S_JITAlerts_ActiveCourse)
    #         time.sleep(5)
    #         learningtemplatecomponentpage.clk_choose_Templates()
    #         time.sleep(20)
    #         Learning_Template_Outline_JIT_Alertspage.clk_Progress()
    #         learningtemplatecomponentpage.chooseunit(TD_JIT_Alerts_Student.S_JITAlerts_Unit)
    #         learningtemplatecomponentpage.Choose_topic(TD_JIT_Alerts_Student.S_JITAlerts_Topic)
    #         learningtemplatecomponentpage.chooseday(TD_JIT_Alerts_Student.S_JITAlerts_Day)
    #         time.sleep(40)
    #         learningtemplatecomponentpage.chooselesson(select_lession1)
    #         time.sleep(20)
    #
    #         # element = self.driver.find_element_by_xpath("//ul[@class='c-editor-pagebreak__nav__pages']")
    #         # log.info("*************")
    #         # pages = element.find_elements_by_xpath("li")
    #         # log.info("$$$$$$")
    #         # totalPage= pages.size()
    #         # log.info("Total Page :",totalPage)
    #         # for page in pages:
    #         #     page.click()
    #         #     log.info("1st page ,1st click")
    #
    #         pape2nd = self.driver.find_element_by_xpath("//li[@class='c-editor-pagebreak__nav__pages-item'][2]/a")
    #         # pagination 1st click
    #         pape2nd.click()
    #
    #         ele_alert_text = self.driver.find_element_by_xpath("//div[@class='Toastify__toast-container Toastify__toast-container--bottom-right jit-alert-wrapper']//div[@class='jit-alert']").text
    #         log.info(ele_alert_text)
    #         # assert("Hey! It looks like you missed a question. Give it a try.",ele_alert_text)
    #         if "Hey!" in ele_alert_text:
    #             if "It looks like you missed a question." in ele_alert_text:
    #                 if "Give it a try." in ele_alert_text:
    #                     log.info("First click on Page2 on " + select_lession1 + "and Validate JIT Alert massage .")
    #             else:
    #                 log.info("First click on Page2 on " + select_lession1 + " and Not Validated JIT Alert massage .")
    #
    #         time.sleep(10)
    #         # pagination 2nd click
    #         pape2nd.click()
    #         pagination = self.driver.find_element_by_xpath(
    #             "//li[@class='c-editor-pagebreak__nav__pages-item'][2]/a")
    #         paginationText2nd = pagination.get_attribute("title")
    #         log.info(paginationText2nd)
    #         if "Page 2" in paginationText2nd:
    #             assert True
    #             log.info("Second click on Page2 and page has been navigated to 2nd page.")
    #
    #         self.driver.execute_script(
    #             'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed"}}')
    #     except:
    #         self.driver.execute_script(
    #             'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed"}}')

    @pytest.mark.parametrize('select_lession2',
                             [
                                 (TD_JIT_Alerts_Student.S_JITAlerts_Lesson_multipleChoice)

                             ]
                             )
    def test_attemptMCQ_JITAlerts_Student_lesson(self, select_lession2):

        try:
            log = self.getLogger()
            loginpage = LoginPage(self.driver)
            homepage = HomePage(self.driver)
            learningpage = LearningPage(self.driver)
            learningtemplatecomponentpage = Learning_Template_Outline_ComponentPage(self.driver)
            learningtemplateoutlineJITcomponentpage2nd = Learning_Template_Outline_JITcomponentPage_2nd(self.driver)
            Learning_Template_Outline_JIT_Alertspage = Learning_Template_Outline_JIT_AlertsPage(self.driver)
            loginpage.enter_username(TD_JIT_Alerts_Student.UseName_Student)
            loginpage.enter_password(TD_JIT_Alerts_Student.Password_Student)
            loginpage.clk_login()
            time.sleep(10)
            log.info("Login Successfully Completed")
            homepage.clk_learningLink()
            time.sleep(10)
            learningpage.clk_ActiveCoursesTab()
            learningpage.SearchActiveCourseSearchbox(TD_JIT_Alerts_Student.S_JITAlerts_ActiveCourse)
            time.sleep(5)
            learningtemplatecomponentpage.clk_choose_Templates()
            time.sleep(20)
            Learning_Template_Outline_JIT_Alertspage.clk_Progress()
            learningtemplatecomponentpage.chooseunit(TD_JIT_Alerts_Student.S_JITAlerts_Unit)
            learningtemplatecomponentpage.Choose_topic(TD_JIT_Alerts_Student.S_JITAlerts_Topic)
            learningtemplatecomponentpage.chooseday(TD_JIT_Alerts_Student.S_JITAlerts_Day)
            time.sleep(40)
            learningtemplatecomponentpage.chooselesson("L0005")
            time.sleep(20)


            #Firest Page MC check
            mcq_ansOptions = self.driver.find_elements_by_xpath(
                "//tr[@data-test-id='answers-group-wrapper-test-id']//div[@class='c-answer-table__checkbox']")
            for option in mcq_ansOptions:
                option.click()
                btn_submit = self.driver.find_element_by_xpath("//button[contains(text(),'Submit')]")
                time.sleep(10)
                btn_submit.click()
                time.sleep(10)
                # Find and click tryAgain button
                # ----------------------
                eles = self.driver.find_elements(By.XPATH, '//button[contains(text(),"Try Again")]')
                count_ele = len(eles)
                print(count_ele)
                if count_ele > 0:
                    time.sleep(10)
                    self.driver.execute_script("arguments[0].click();", WebDriverWait(self.driver, 30).until(
                        EC.element_to_be_clickable((By.XPATH, '//button[contains(text(),"Try Again")]'))))
                    log.info("**************************")


            Learning_Template_Outline_JIT_Alertspage.clk_continue()
            time.sleep(10)
            # Second Page MC check
            mcq_ansOptions = self.driver.find_elements_by_xpath(
                "//tr[@data-test-id='answers-group-wrapper-test-id']//div[@class='c-answer-table__checkbox']")
            for option in mcq_ansOptions:
                option.click()
                btn_submit = self.driver.find_element_by_xpath("//button[contains(text(),'Submit')]")
                time.sleep(10)
                btn_submit.click()
                time.sleep(10)
                # Find and click tryAgain button
                # ----------------------
                eles = self.driver.find_elements(By.XPATH, '//button[contains(text(),"Try Again")]')
                count_ele = len(eles)
                print(count_ele)
                if count_ele > 0:
                    time.sleep(10)
                    self.driver.execute_script("arguments[0].click();", WebDriverWait(self.driver, 30).until(
                        EC.element_to_be_clickable((By.XPATH, '//button[contains(text(),"Try Again")]'))))
                    log.info("**************************")
            Learning_Template_Outline_JIT_Alertspage.clk_continue()
            # --------------------------------- Verefy Alerts is present and Validated Text
            ele_alert= self.driver.find_elements_by_xpath(
                "//div[@class='Toastify__toast-container Toastify__toast-container--bottom-right jit-alert-wrapper']//div[@class='jit-alert']")
            count_ele_alert = len(ele_alert)
            print(count_ele_alert)
            if count_ele_alert > 0:
                ele_alert_text=ele_alert.text
                # assert("Hey! It looks like you missed a question. Give it a try.",ele_alert_text)
                if ("Hey!" and "It looks like you missed a question." and "Give it a try.") in ele_alert_text:
                    log.info(" click on continue on " + select_lession2 + " and Validate JIT Alert massage .")
                else:
                    log.info("click on continue on " + select_lession2 + " and Not Validated JIT Alert massage .")
            # --------------------------------------
            time.sleep(10)
            msgPopup =self.driver.find_element_by_xpath("//h5[contains(text(),'Keep it up!')]").text
            log.info("---------------"+ msgPopup)
            type(msgPopup)
            assert(msgPopup,"Keep it up!","Verify Popup Massage ")
            time.sleep(5)


            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed"}}')
        except:

            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed"}}')
