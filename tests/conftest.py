import pytest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import time

from data.TD_login import TD_login


driver = None


def pytest_addoption(parser):
    parser.addoption(
        "--browser_name", action="store", default="chrome"
    )
    parser.addoption(
        "--os", action="store", default="Windows"
    )


@pytest.fixture()
def OneTimesetup(request):
    desired_cap = {
        'browserstack.debug': 'true',
        "browserstack.console": "info",
        'browserName': request.config.getoption("browser_name"),
        'os': request.config.getoption("os"),
        'os_version': '10',
        'name': 'EDIO-JIT Alert',  # test name
        'build': 'QA_PAL Build'  # CI/CD job or build name
    }
    global driver
    driver = webdriver.Remote(
        command_executor='https://tanmoyray1:6eLzZuNdMdHEU19e9UHm@hub-cloud.browserstack.com/wd/hub',
        desired_capabilities=desired_cap)
    driver.get(TD_login.baseurl)
    driver.implicitly_wait(30)
    driver.maximize_window()
    request.cls.driver = driver
    yield
    driver.quit()




