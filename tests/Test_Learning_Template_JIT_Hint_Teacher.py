import time

from selenium.webdriver.support.wait import WebDriverWait

from config.BaseClass import BaseClass
from data.TD_JIT_Hint_Teacher import TD_JIT_Hint_Teacher

from pages.HomePage import HomePage
from pages.LearningPage import LearningPage
from pages.Learning_Template_Outline_ComponentPage import Learning_Template_Outline_ComponentPage
from pages.Learning_Template_Outline_JITcomponentPage_2nd import Learning_Template_Outline_JITcomponentPage_2nd

from pages.LoginPage import LoginPage


class Test_Learning_Template_JIT_Hint_Teacher(BaseClass):

    def test_validate_hint_Sketchpad_label_Teacher_lesson(self):
        try:
            log = self.getLogger()
            loginpage = LoginPage(self.driver)
            homepage = HomePage(self.driver)
            learningpage = LearningPage(self.driver)
            learningtemplatecomponentpage = Learning_Template_Outline_ComponentPage(self.driver)
            learningtemplateoutlineJITcomponentpage2nd = Learning_Template_Outline_JITcomponentPage_2nd(self.driver)
            loginpage.enter_username(TD_JIT_Hint_Teacher.UseName_Teacher)
            loginpage.enter_password(TD_JIT_Hint_Teacher.Password_Teacher)
            loginpage.clk_login()
            log.info("Login Successfully Completed")
            homepage.clk_learningLink()
            learningpage.clk_ActiveCoursesTab()
            learningpage.SearchActiveCourseSearchbox(TD_JIT_Hint_Teacher.T_JITHint_ActiveCourse)
            learningtemplatecomponentpage.clk_choose_Templates()
            learningtemplateoutlineJITcomponentpage2nd.clk_Course_Outline()
            learningtemplatecomponentpage.chooseunit(TD_JIT_Hint_Teacher.T_JITHint_Unit)
            learningtemplatecomponentpage.chooseday(TD_JIT_Hint_Teacher.T_JITHint_Day)
            time.sleep(40)
            learningtemplatecomponentpage.chooselesson(TD_JIT_Hint_Teacher.T_JITHint_Lesson_Sketchpad)
            time.sleep(20)
            learningtemplateoutlineJITcomponentpage2nd.clk_content()
            time.sleep(10)
            learningtemplateoutlineJITcomponentpage2nd.clk_editcontent()
            time.sleep(5)
            learningtemplatecomponentpage.Search_Item_By_Text(TD_JIT_Hint_Teacher.T_JITHint_SearchBySketchpad)
            time.sleep(5)
            learningtemplatecomponentpage.draganddropelement()
            time.sleep(10)
            learningtemplatecomponentpage.clk_edit_button()
            time.sleep(5)
            # learningtemplateoutlineJITcomponentpage2nd.upperdetails_hotspotlabel()
            # learningtemplateoutlineJITcomponentpage2nd.validate_hint_settings()
            # learningtemplateoutlineJITcomponentpage2nd.validate_first_hint()

            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed"}}')

        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed"}}')



    def validate_hint_hotspot_label_Teacher_lesson(self):
        try:
            log = self.getLogger()
            loginpage = LoginPage(self.driver)
            homepage = HomePage(self.driver)
            learningpage = LearningPage(self.driver)
            learningtemplatecomponentpage = Learning_Template_Outline_ComponentPage(self.driver)
            learningtemplateoutlineJITcomponentpage2nd = Learning_Template_Outline_JITcomponentPage_2nd(self.driver)
            loginpage.enter_username(TD_JIT_Hint_Teacher.UseName_Teacher)
            loginpage.enter_password(TD_JIT_Hint_Teacher.Password_Teacher)
            loginpage.clk_login()
            log.info("Login Successfully Completed")
            homepage.clk_learningLink()
            learningpage.clk_ActiveCoursesTab()
            learningpage.SearchActiveCourseSearchbox(TD_JIT_Hint_Teacher.T_JITHint_ActiveCourse)
            learningtemplatecomponentpage.clk_choose_Templates()
            learningtemplateoutlineJITcomponentpage2nd.clk_Course_Outline()
            learningtemplatecomponentpage.chooseunit(TD_JIT_Hint_Teacher.T_JITHint_Unit)
            learningtemplatecomponentpage.chooseday(TD_JIT_Hint_Teacher.T_JITHint_Day)
            time.sleep(40)
            learningtemplatecomponentpage.chooselesson(TD_JIT_Hint_Teacher.T_JITHint_Lesson)
            time.sleep(20)
            learningtemplateoutlineJITcomponentpage2nd.clk_content()
            time.sleep(10)
            learningtemplateoutlineJITcomponentpage2nd.clk_editcontent()
            time.sleep(5)
            learningtemplatecomponentpage.Search_Item_By_Text()
            time.sleep(5)
            learningtemplatecomponentpage.draganddropelement()
            time.sleep(10)
            learningtemplatecomponentpage.clk_edit_button()
            time.sleep(5)
            learningtemplateoutlineJITcomponentpage2nd.upperdetails_hotspotlabel()
            learningtemplateoutlineJITcomponentpage2nd.validate_hint_settings()
            learningtemplateoutlineJITcomponentpage2nd.validate_first_hint()

            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed"}}')

        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed"}}')

    def Add_hint_hotspot_label_Teacher_lesson(self):
        try:

            log = self.getLogger()
            loginpage = LoginPage(self.driver)
            homepage = HomePage(self.driver)
            learningpage = LearningPage(self.driver)
            learningtemplatecomponentpage = Learning_Template_Outline_ComponentPage(self.driver)
            learningtemplateoutlineJITcomponentpage2nd = Learning_Template_Outline_JITcomponentPage_2nd(self.driver)

            loginpage.enter_username(TD_JIT_Hint_Teacher.UseName_Teacher)
            loginpage.enter_password(TD_JIT_Hint_Teacher.Password_Teacher)
            loginpage.clk_login()
            log.info("Login Successfully Completed")
            homepage.clk_learningLink()
            learningpage.clk_ActiveCoursesTab()
            learningpage.SearchActiveCourseSearchbox()
            learningtemplatecomponentpage.clk_choose_Templates()
            learningtemplateoutlineJITcomponentpage2nd.clk_Course_Outline()
            learningtemplatecomponentpage.chooseunit(TD_JIT_Hint_Teacher.T_JITHint_Unit)
            learningtemplatecomponentpage.chooseday(TD_JIT_Hint_Teacher.T_JITHint_Day)
            time.sleep(40)
            learningtemplatecomponentpage.chooselesson(TD_JIT_Hint_Teacher.T_JITHint_Lesson)
            time.sleep(20)
            learningtemplateoutlineJITcomponentpage2nd.clk_content()
            time.sleep(10)
            learningtemplateoutlineJITcomponentpage2nd.clk_editcontent()
            time.sleep(5)
            learningtemplatecomponentpage.Search_Item_By_Text()
            time.sleep(5)
            learningtemplatecomponentpage.draganddropelement()
            time.sleep(10)
            learningtemplatecomponentpage.clk_edit_button()
            time.sleep(5)
            learningtemplateoutlineJITcomponentpage2nd.upperdetails_hotspotlabel()
            learningtemplateoutlineJITcomponentpage2nd.add_hint()
            learningtemplateoutlineJITcomponentpage2nd.clk_apply_changes()

            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed"}}')

        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed"}}')

    def validate_empty_hint_hotspot_label_Teacher_lesson(self):
        try:

            log = self.getLogger()
            loginpage = LoginPage(self.driver)
            homepage = HomePage(self.driver)
            learningpage = LearningPage(self.driver)
            learningtemplatecomponentpage = Learning_Template_Outline_ComponentPage(self.driver)
            learningtemplateoutlineJITcomponentpage2nd = Learning_Template_Outline_JITcomponentPage_2nd(self.driver)

            loginpage.enter_username(TD_JIT_Hint_Teacher.UseName_Teacher)
            loginpage.enter_password(TD_JIT_Hint_Teacher.Password_Teacher)
            loginpage.clk_login()
            log.info("Login Successfully Completed")
            homepage.clk_learningLink()
            learningpage.clk_ActiveCoursesTab()
            learningpage.SearchActiveCourseSearchbox()
            learningtemplatecomponentpage.clk_choose_Templates()
            learningtemplateoutlineJITcomponentpage2nd.clk_Course_Outline()
            learningtemplatecomponentpage.chooseunit(TD_JIT_Hint_Teacher.T_JITHint_Unit)
            learningtemplatecomponentpage.chooseday(TD_JIT_Hint_Teacher.T_JITHint_Day)
            time.sleep(40)
            learningtemplatecomponentpage.chooselesson(TD_JIT_Hint_Teacher.T_JITHint_Lesson)
            time.sleep(20)
            learningtemplateoutlineJITcomponentpage2nd.clk_content()
            time.sleep(10)
            learningtemplateoutlineJITcomponentpage2nd.clk_editcontent()
            time.sleep(5)
            learningtemplatecomponentpage.Search_Item_By_Text()
            time.sleep(5)
            learningtemplatecomponentpage.draganddropelement()
            time.sleep(10)
            learningtemplatecomponentpage.clk_edit_button()
            time.sleep(5)
            learningtemplateoutlineJITcomponentpage2nd.upperdetails_hotspotlabel()
            learningtemplateoutlineJITcomponentpage2nd.validate_hintsettings_with_one_empty_hint()

            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed"}}')

        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed"}}')

    #Not tested
    def validate_hint_copy_hotspotlabel_Teacher_lesson(self):
        try:

            log = self.getLogger()
            loginpage = LoginPage(self.driver)
            homepage = HomePage(self.driver)
            learningpage = LearningPage(self.driver)
            learningtemplatecomponentpage = Learning_Template_Outline_ComponentPage(self.driver)
            learningtemplateoutlineJITcomponentpage2nd = Learning_Template_Outline_JITcomponentPage_2nd(self.driver)

            loginpage.enter_username(TD_JIT_Hint_Teacher.UseName_Teacher)
            loginpage.enter_password(TD_JIT_Hint_Teacher.Password_Teacher)
            loginpage.clk_login()
            log.info("Login Successfully Completed")
            homepage.clk_learningLink()
            learningpage.clk_ActiveCoursesTab()
            time.sleep(10)
            learningpage.SearchActiveCourseSearchbox(TD_JIT_Hint_Teacher.T_JITHint_ActiveCourse)
            learningtemplatecomponentpage.clk_choose_Templates()
            learningtemplateoutlineJITcomponentpage2nd.clk_Course_Outline()
            learningtemplatecomponentpage.chooseunit(TD_JIT_Hint_Teacher.T_JITHint_Unit)
            learningtemplatecomponentpage.chooseday(TD_JIT_Hint_Teacher.T_JITHint_Day)
            time.sleep(40)
            learningtemplatecomponentpage.chooselesson(TD_JIT_Hint_Teacher.T_JITHint_Lesson)
            time.sleep(20)
            learningtemplateoutlineJITcomponentpage2nd.clk_content()
            time.sleep(10)
            learningtemplateoutlineJITcomponentpage2nd.clk_editcontent()
            time.sleep(5)
            learningtemplatecomponentpage.Search_Item_By_Text()
            time.sleep(5)
            learningtemplatecomponentpage.draganddropelement()
            time.sleep(10)
            learningtemplatecomponentpage.clk_edit_button()
            time.sleep(5)
            learningtemplateoutlineJITcomponentpage2nd.upperdetails_hotspotlabel()
            #learningtemplateoutlineJITcomponentpage2nd.validate_hintsettings_with_one_empty_hint()
            learningtemplateoutlineJITcomponentpage2nd.validate_hint_settings()
            learningtemplateoutlineJITcomponentpage2nd.validate_first_hint()
            learningtemplateoutlineJITcomponentpage2nd.copy_component("Hotspot Label")
            learningtemplatecomponentpage.search_item().clear()
            time.sleep(5)
            learningtemplatecomponentpage.search_item.send_keys("paste ID")
            learningtemplatecomponentpage.decision_draganddrop()
            time.sleep(5)
            learningtemplatecomponentpage.edit_button_click()
            time.sleep(5)
            learningtemplateoutlineJITcomponentpage2nd.validate_copy_component("Hotspot Label")







            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed"}}')

        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed"}}')











