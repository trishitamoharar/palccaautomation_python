import time

import pytest
from selenium.webdriver.support.wait import WebDriverWait

from config.BaseClass import BaseClass
from data import TD_JIT_Alerts_Student
from data.TD_JIT_Hint_Teacher import TD_JIT_Hint_Teacher

from pages.HomePage import HomePage
from pages.LearningPage import LearningPage
from pages.Learning_Template_Outline_ComponentPage import Learning_Template_Outline_ComponentPage
from pages.Learning_Template_Outline_JIT_AlertsPage import Learning_Template_Outline_JIT_AlertsPage
from pages.Learning_Template_Outline_JITcomponentPage_2nd import Learning_Template_Outline_JITcomponentPage_2nd

from pages.LoginPage import LoginPage


# Pre condition :
# Student data should be minimum  2 page and one page with one component
# and no answer was submit before

class Test_Learning_Template_JIT_Alerts_Student(BaseClass):

    @pytest.mark.parametrize('select_lession',
                             [
                                 (TD_JIT_Alerts_Student.S_JITAlerts_Lesson_audioAnswer),
                                 (TD_JIT_Alerts_Student.S_JITAlerts_Lesson_hotspotLabel),
                                 (TD_JIT_Alerts_Student.S_JITAlerts_Lesson_multipleChoice),
                                 (TD_JIT_Alerts_Student.S_JITAlerts_Lesson_hotspotAnswer),
                                 (TD_JIT_Alerts_Student.S_JITAlerts_Lesson_longAnswer),
                                 (TD_JIT_Alerts_Student.S_JITAlerts_Lesson_matchingList),
                                 (TD_JIT_Alerts_Student.S_JITAlerts_Lesson_category),
                                 (TD_JIT_Alerts_Student.S_JITAlerts_Lesson_sortableList),
                                 (TD_JIT_Alerts_Student.S_JITAlerts_Lesson_discussionAnswer),
                                 (TD_JIT_Alerts_Student.S_JITAlerts_Lesson_fileUpload),
                                 (TD_JIT_Alerts_Student.S_JITAlerts_Lesson_shortAnswer),
                                 (TD_JIT_Alerts_Student.S_JITAlerts_Lesson_videoAnswer),
                                 (TD_JIT_Alerts_Student.S_JITAlerts_Lesson_sketchpad)

                             ]
                             )

    def test_FirstandSecont_tattempt_validateOnContinueBtn_JITAlerts_Student_lesson(self,select_lession):
        try:
            log = self.getLogger()
            loginpage = LoginPage(self.driver)
            homepage = HomePage(self.driver)
            learningpage = LearningPage(self.driver)
            learningtemplatecomponentpage = Learning_Template_Outline_ComponentPage(self.driver)
            learningtemplateoutlineJITcomponentpage2nd = Learning_Template_Outline_JITcomponentPage_2nd(self.driver)
            Learning_Template_Outline_JIT_Alertspage = Learning_Template_Outline_JIT_AlertsPage(self.driver)
            loginpage.enter_username(TD_JIT_Alerts_Student.UseName_Student)
            loginpage.enter_password(TD_JIT_Alerts_Student.Password_Student)
            loginpage.clk_login()
            time.sleep(10)
            log.info("Login Successfully Completed")
            homepage.clk_learningLink()
            time.sleep(10)
            learningpage.clk_ActiveCoursesTab()
            learningpage.SearchActiveCourseSearchbox(TD_JIT_Alerts_Student.S_JITAlerts_ActiveCourse)
            time.sleep(5)
            learningtemplatecomponentpage.clk_choose_Templates()
            time.sleep(20)
            Learning_Template_Outline_JIT_Alertspage.clk_Progress()
            learningtemplatecomponentpage.chooseunit(TD_JIT_Alerts_Student.S_JITAlerts_Unit)
            learningtemplatecomponentpage.Choose_topic(TD_JIT_Alerts_Student.S_JITAlerts_Topic)
            learningtemplatecomponentpage.chooseday(TD_JIT_Alerts_Student.S_JITAlerts_Day)
            time.sleep(40)
            learningtemplatecomponentpage.chooselesson(select_lession)
            time.sleep(20)

            # 1st click on continue , JIT Alert validate and Page has not been navigated to 2nd page
            Learning_Template_Outline_JIT_Alertspage.clk_continue()
            # Learning_Template_Outline_JIT_Alertspage.First_click_on_continue_and_Validate_JIT_Alert_massage()
            ele_alert_text = self.driver.find_element_by_xpath("//div[@class='Toastify__toast-container Toastify__toast-container--bottom-right jit-alert-wrapper']//div[@class='jit-alert']").text
            log.info(ele_alert_text)
            # assert("Hey! It looks like you missed a question. Give it a try.",ele_alert_text)
            if("Hey!" and "It looks like you missed a question." and "Give it a try.") in ele_alert_text:
                log.info("First click on continue on "+select_lession +" and Validate JIT Alert massage .")
            else:
                log.info("First click on continue on "+select_lession +" and Not Validated JIT Alert massage .")

            # 2nd click on continue , Page has been navigated to 2nd page
            time.sleep(10)
            Learning_Template_Outline_JIT_Alertspage.clk_continue()
            Learning_Template_Outline_JIT_Alertspage.Second_click_on_continue_Page_has_been_navigated_to_2nd_page()

            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed"}}')
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed"}}')

    @pytest.mark.parametrize('select_lession1',
                             [
                                 (TD_JIT_Alerts_Student.S_JITAlerts_Lesson_audioAnswer),
                                 (TD_JIT_Alerts_Student.S_JITAlerts_Lesson_hotspotLabel),
                                 (TD_JIT_Alerts_Student.S_JITAlerts_Lesson_multipleChoice),
                                 (TD_JIT_Alerts_Student.S_JITAlerts_Lesson_hotspotAnswer),
                                 (TD_JIT_Alerts_Student.S_JITAlerts_Lesson_longAnswer),
                                 (TD_JIT_Alerts_Student.S_JITAlerts_Lesson_matchingList),
                                 (TD_JIT_Alerts_Student.S_JITAlerts_Lesson_category),
                                 (TD_JIT_Alerts_Student.S_JITAlerts_Lesson_sortableList),
                                 (TD_JIT_Alerts_Student.S_JITAlerts_Lesson_discussionAnswer),
                                 (TD_JIT_Alerts_Student.S_JITAlerts_Lesson_fileUpload),
                                 (TD_JIT_Alerts_Student.S_JITAlerts_Lesson_shortAnswer),
                                 (TD_JIT_Alerts_Student.S_JITAlerts_Lesson_videoAnswer),
                                 (TD_JIT_Alerts_Student.S_JITAlerts_Lesson_sketchpad)

                             ]
                             )

    def test_FirstandSecond_tattempt_validateOnPagination_JITAlerts_Student_lesson(self,select_lession1):
        try:
            log = self.getLogger()
            loginpage = LoginPage(self.driver)
            homepage = HomePage(self.driver)
            learningpage = LearningPage(self.driver)
            learningtemplatecomponentpage = Learning_Template_Outline_ComponentPage(self.driver)
            learningtemplateoutlineJITcomponentpage2nd = Learning_Template_Outline_JITcomponentPage_2nd(self.driver)
            Learning_Template_Outline_JIT_Alertspage = Learning_Template_Outline_JIT_AlertsPage(self.driver)
            loginpage.enter_username(TD_JIT_Alerts_Student.UseName_Student)
            loginpage.enter_password(TD_JIT_Alerts_Student.Password_Student)
            loginpage.clk_login()
            time.sleep(10)
            log.info("Login Successfully Completed")
            homepage.clk_learningLink()
            time.sleep(10)
            learningpage.clk_ActiveCoursesTab()
            learningpage.SearchActiveCourseSearchbox(TD_JIT_Alerts_Student.S_JITAlerts_ActiveCourse)
            time.sleep(5)
            learningtemplatecomponentpage.clk_choose_Templates()
            time.sleep(20)
            Learning_Template_Outline_JIT_Alertspage.clk_Progress()
            learningtemplatecomponentpage.chooseunit(TD_JIT_Alerts_Student.S_JITAlerts_Unit)
            learningtemplatecomponentpage.Choose_topic(TD_JIT_Alerts_Student.S_JITAlerts_Topic)
            learningtemplatecomponentpage.chooseday(TD_JIT_Alerts_Student.S_JITAlerts_Day)
            time.sleep(40)
            learningtemplatecomponentpage.chooselesson(select_lession1)
            time.sleep(20)

            pape2nd = self.driver.find_element_by_xpath("//li[@class='c-editor-pagebreak__nav__pages-item'][2]/a")
            # pagination 1st click
            pape2nd.click()
            ele_alert_text = self.driver.find_element_by_xpath("//div[@class='Toastify__toast-container Toastify__toast-container--bottom-right jit-alert-wrapper']//div[@class='jit-alert']").text
            log.info(ele_alert_text)
            # assert("Hey! It looks like you missed a question. Give it a try.",ele_alert_text)
            if "Hey!" in ele_alert_text:
                if "It looks like you missed a question." in ele_alert_text:
                    if "Give it a try." in ele_alert_text:
                        log.info("First click on Page2 on "+select_lession1 +"and Validate JIT Alert massage .")
            else:
                log.info("First click on Page2 on "+select_lession1 +" and Not Validated JIT Alert massage .")

            time.sleep(10)
            # pagination 2nd click
            pape2nd.click()
            pagination = self.driver.find_element_by_xpath(
                "//li[@class='c-editor-pagebreak__nav__pages-item'][2]/a")
            paginationText2nd = pagination.get_attribute("title")
            log.info(paginationText2nd)
            if "Page 2" in paginationText2nd:
                assert True
                log.info("Second click on Page2 and page has been navigated to 2nd page.")

            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed"}}')
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed"}}')

    # Not Tested
    #Not Required 
    def OnPendingAnswer_JITAlerts_hotspot_label_Student_lesson(self):
        try:
            log = self.getLogger()
            loginpage = LoginPage(self.driver)
            homepage = HomePage(self.driver)
            learningpage = LearningPage(self.driver)
            learningtemplatecomponentpage = Learning_Template_Outline_ComponentPage(self.driver)
            learningtemplateoutlineJITcomponentpage2nd = Learning_Template_Outline_JITcomponentPage_2nd(self.driver)
            Learning_Template_Outline_JIT_Alertspage = Learning_Template_Outline_JIT_AlertsPage(self.driver)
            loginpage.enter_username(TD_JIT_Alerts_Student.UseName_Student)
            loginpage.enter_password(TD_JIT_Alerts_Student.Password_Student)
            loginpage.clk_login()
            log.info("Login Successfully Completed")
            homepage.clk_learningLink()
            learningpage.clk_ActiveCoursesTab()
            learningpage.SearchActiveCourseSearchbox(TD_JIT_Alerts_Student.S_JITAlerts_ActiveCourse)
            time.sleep(5)
            learningtemplatecomponentpage.clk_choose_Templates()
            time.sleep(20)
            Learning_Template_Outline_JIT_Alertspage.clk_Progress()
            learningtemplatecomponentpage.chooseunit(TD_JIT_Alerts_Student.S_JITAlerts_Unit)
            learningtemplatecomponentpage.Choose_topic(TD_JIT_Alerts_Student.S_JITAlerts_Topic)
            learningtemplatecomponentpage.chooseday(TD_JIT_Alerts_Student.S_JITAlerts_Day)
            time.sleep(40)
            learningtemplatecomponentpage.chooselesson(TD_JIT_Hint_Teacher.T_JITHint_Lesson)
            time.sleep(20)

            # filled all vaid data and click on submit -->continue
            elements = self.driver.find_element_by_xpath("//p[@class=' c-editor-hotspot__text -small']")
            log.info(len(elements))
            for i in elements:
                self.driver.find_element_by_xpath("//p[@class=' c-editor-hotspot__text -small']").click()
                time.sleep(5)
                # //ul[@class='c-editor-hotspot-label__labels']/li[1]//input
                txt_elements = self.driver.find_element_by_xpath("//")

            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed"}}')
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed"}}')


