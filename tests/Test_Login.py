import pytest
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.by import By

from config.BaseClass import BaseClass
from data.TD_login import TD_login
from pages.LoginPage import LoginPage
from tests.conftest import driver


class Test_Login(BaseClass):

    def test_validlogin(self):
        try:
            log = self.getLogger()
            loginpage = LoginPage(self.driver)
            loginpage.enter_username()
            loginpage.enter_password()
            loginpage.clk_login()
            log.info("Login Successfully Completed")

            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed"}}')

        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed"}}')
