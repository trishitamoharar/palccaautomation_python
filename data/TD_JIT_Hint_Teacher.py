class TD_JIT_Hint_Teacher:
    UseName_Teacher="jlipchik"
    Password_Teacher="Passw0rd!"

    T_JITHint_ActiveCourse="World History"
    T_JITHint_Unit = "The Cold War"
    T_JITHint_Day = "Cold War Test"
    T_JITHint_Lesson = "L1"
    T_JITHint_Lesson_Sketchpad = "L3"

    T_JITHint_SearchByHotspotlabel= "Hotspot Label"
    T_JITHint_SearchBySketchpad= "Sketchpad"
