from selenium.webdriver.common.by import By

from config.BaseClass import BaseClass
from data.TD_login import TD_login
from pages.HomePage import HomePage


class LoginPage():

    def __init__(self, driver):
        self.driver = driver

        self.username = "username"
        self.password = "password"
        self.login = "c-button"

    def enter_username(self,username):
        return self.driver.find_element_by_id(self.username).send_keys(username)

    def enter_password(self,password):
        return self.driver.find_element_by_id(self.password).send_keys(password)

    def clk_login(self):
        self.driver.find_element_by_class_name(self.login).click()
        homepage = HomePage(self.driver)
        return homepage
