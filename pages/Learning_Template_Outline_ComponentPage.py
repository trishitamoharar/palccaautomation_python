import time

from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By

from config.BaseClass import BaseClass
from data.TD_JIT_Hint_Teacher import TD_JIT_Hint_Teacher


class Learning_Template_Outline_ComponentPage:
    def __init__(self, driver):
        self.driver = driver
        self.choose_templates="//div[@class='c-course-list']/div[1]/div/div[2]/div/a/h5"
        self.search_item="//input[@placeholder='Search for a Component']"
        self.edit_button="//button[text()='Edit']"
        self.source="//div[@class=\"c-page-sidebar\"]/div/div[2]/div[2]/div/div"
        self.Target="//div[@class=\"c-page-content -neutral-lightest\"]/section/section/div/div/div/div"
        self.Target_1="//div[@class=\"c-page-content -neutral-lightest\"]/section/section/div/div/div"


    def source(self):
        return self.driver.find_element_by_xpath(self.source)

    def Target(self):
        return self.driver.find_element_by_xpath(self.Target)

    def Target_1(self):
        return self.driver.find_element_by_xpath(self.Target_1)

    def clk_choose_Templates(self):
        return self.driver.find_element_by_xpath(self.choose_templates).click()

    def edit_button_click(self):
        return self.driver.find_element_by_xpath(self.edit_button).click()

    def chooseunit(self, unitname):
        before_unit = "//label[text()='"
        after_unit = "']/parent::div/parent::div/parent::div/preceding-sibling::div[2]/button/i"
        unit_click = self.driver.find_element_by_xpath(before_unit + unitname + after_unit)
        unit_click.click()

    def Choose_topic(self,topicname):
        before_topic="//label[text()='"
        after_topic="']/parent::div/parent::div/parent::div/preceding-sibling::div[2]/button/i"
        topic_click=self.driver.find_element_by_xpath(before_topic+topicname+after_topic)
        topic_click.click()

    def chooseday(self,dayname):
        before_day = "//label[text()='"
        after_day = "']/parent::a"
        day_click = self.driver.find_element_by_xpath(before_day + dayname + after_day)
        day_click.click()

    def chooselesson(self,lesson_name):
        ele = self.driver.find_element_by_xpath("//div[@class='c-day-sidebar__items']")
        less_name = lesson_name
        before_lessname = "//div[@class='c-day-sidebar__item-label']/i[@class='c-icon -lesson']/parent::div/following-sibling::p[text()='"
        Click_lesson = ele.find_element_by_xpath(before_lessname + less_name + "']")
        self.driver.execute_script("arguments[0].scrollIntoView(true);", Click_lesson)
        time.sleep(5)
        Click_lesson.click()

    def Search_Item_By_Text(self, searchByText):
        return self.driver.find_element_by_xpath(self.search_item).send_keys(searchByText)

    def DragAndDropJS(self, driver, source, target):
        self.driver.execute_script("function createEvent(typeOfEvent)"
                                   + " {\n" + "var event =document.createEvent(\"CustomEvent\");\n"
                                   + "event.initCustomEvent(typeOfEvent,true, true, null);\n"
                                   + "event.dataTransfer = {\n" + "data: {},\n"
                                   + "setData: function (key, value) {\n" + "this.data[key] = value;\n" + "},\n"
                                   + "getData: function (key) {\n" + "return this.data[key];\n" + "}\n" + "};\n"
                                   + "return event;\n" + "}\n" + "\n" + "function dispatchEvent(element, event,transferData) {\n"
                                   + "if (transferData !== undefined) {\n" + "event.dataTransfer = transferData;\n" + "}\n"
                                   + "if (element.dispatchEvent) {\n" + "element.dispatchEvent(event);\n"
                                   + "} else if (element.fireEvent) {\n" + "element.fireEvent(\"on\" + event.type, event);\n"
                                   + "}\n" + "}\n" + "\n" + "function simulateHTML5DragAndDrop(element, destination) {\n"
                                   + "var dragStartEvent =createEvent('dragstart');\n" + "dispatchEvent(element, dragStartEvent);\n"
                                   + "var dropEvent = createEvent('drop');\n"
                                   + "dispatchEvent(destination, dropEvent,dragStartEvent.dataTransfer);\n"
                                   + "var dragEndEvent = createEvent('dragend');\n"
                                   + "dispatchEvent(element, dragEndEvent,dropEvent.dataTransfer);\n"
                                   + "}\n" + "\n" + "var source = arguments[0];\n"
                                   + "var destination = arguments[1];\n" + "simulateHTML5DragAndDrop(source,destination);",
                                   source, target)

    def draganddropelement(self):
        try:
            self.driver.find_element_by_xpath("//i[@class='c-icon -copy']")
            present = "true"
        except NoSuchElementException:
            present = "false"

            if present == "false":
                source = self.driver.find_element_by_xpath("//div[@class='c-page-sidebar']/div/div[2]/div[2]/div/div")
                target = self.driver.find_element_by_xpath("//section[@class='c-wrapper']//i")
                self.DragAndDropJS(self.driver, source, target)
            else:
                #self.driver.find_element_by_xpath("//div[@class='c-editor-component__actions']//i").click()
                #self.driver.find_element_by_xpath("//div[@class='c-modal__actions']/button[2]").click()
                #time.sleep(5)
                source = self.driver.find_element_by_xpath("//div[@class='c-page-sidebar']/div/div[2]/div[2]/div/div")
                target = self.driver.find_element_by_xpath("//section[@class='c-wrapper']//i")
                self.DragAndDropJS(self.driver,source, target )

    def clk_edit_button(self):
        return self.driver.find_element_by_xpath(self.edit_button).click()

    def decision_draganddrop(self):
        try:
            self.driver.find_element_by_xpath("//i[@class='c-icon -copy']")
            present = True
        except NoSuchElementException:
            present = False

        print("the value is " + present)
        checkval = str(present)
        print("the value of checkval is " + checkval)
        if checkval== False:
                self.DragAndDropJS_first(self.source,self.Target_1, self.driver)
        else:
                self.DragAndDropJS(self.source,self.Target, self.driver)

    def DragAndDropJS_first(self, driver, source, Target_1):
        self.driver.execute_script("function createEvent(typeOfEvent)"
                         + " {\n" + "var event =document.createEvent(\"CustomEvent\");\n"
                         + "event.initCustomEvent(typeOfEvent,true, true, null);\n"
                         + "event.dataTransfer = {\n" + "data: {},\n"
                         + "setData: function (key, value) {\n" + "this.data[key] = value;\n" + "},\n"
                         + "getData: function (key) {\n" + "return this.data[key];\n" + "}\n" + "};\n"
                         + "return event;\n" + "}\n" + "\n" + "function dispatchEvent(element, event,transferData) {\n"
                         + "if (transferData !== undefined) {\n" + "event.dataTransfer = transferData;\n" + "}\n"
                         + "if (element.dispatchEvent) {\n" + "element.dispatchEvent(event);\n"
                         + "} else if (element.fireEvent) {\n" + "element.fireEvent(\"on\" + event.type, event);\n"
                         + "}\n" + "}\n" + "\n" + "function simulateHTML5DragAndDrop(element, destination) {\n"
                         + "var dragStartEvent =createEvent('dragstart');\n" + "dispatchEvent(element, dragStartEvent);\n"
                         + "var dropEvent = createEvent('drop');\n"
                         + "dispatchEvent(destination, dropEvent,dragStartEvent.dataTransfer);\n"
                         + "var dragEndEvent = createEvent('dragend');\n"
                         + "dispatchEvent(element, dragEndEvent,dropEvent.dataTransfer);\n"
                         + "}\n" + "\n" + "var source = arguments[0];\n"
                         + "var destination = arguments[1];\n" + "simulateHTML5DragAndDrop(source,destination);",
                         source, Target_1)
        time.sleep(5)











