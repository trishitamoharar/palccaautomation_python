from selenium.webdriver.common.by import By



class HomePage:
    def __init__(self, driver):
        self.driver = driver

        self.learningLink = "//*[@class='c-navigation-wrapper']//ul/li[2]/a"
        self.Profile ="//div[@class='c-sidebar__profile-container']"
        self.Profile_inner="div.c-dropdown.c-profile__body__roles-dropdown > div:nth-child(1) > button"

    def clk_learningLink(self):
        return self.driver.find_element_by_xpath(self.learningLink).click()

    def clk_profile(self):
        return self.driver.find_element_by_xpath(self.Profile).click()

    def clk_Profile_inner(self):
        return self.driver.find_element_by_css_selector(self.Profile_inner).click()


    def ProfileChange_AdminToTeacher(self):
        HomePage.clk_profile()
        HomePage.clk_Profile_inner()
        elements = self.driver.find_element_by_xpath("//div[@class='c-tooltip o-align-left -left -inverted']/div/div/button/span")
        print(elements)
        for i in elements:
            print(elements.get(i).text)
            if elements.get(i).text().contains("TEACHER"):
                elements.get(i).click()

