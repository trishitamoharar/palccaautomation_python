import os
import time

from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys


class Learning_Template_Outline_JITcomponentPage_2nd:
    def __init__(self, driver):
        self.driver = driver
        self.course_outline="//a[text()='Course Outline']"
        self.content="//div[2]/div[2]/div[1]/ul/li[2]/a"
        #self.content="*//a[text()='Content']"
        self.editcontent="//span[text()='Edit Content']/parent::button/i"
        self.apply_changes="//button[text()='Apply Changes']"



    def clk_Course_Outline(self):
        return self.driver.find_element_by_xpath(self.course_outline).click()

    def clk_content(self):
        return self.driver.find_element_by_xpath(self.content).click()

    def clk_editcontent(self):
        return self.driver.find_element_by_xpath(self.editcontent).click()

    def clk_apply_changes(self):
        return self.driver.find_element_by_xpath(self.apply_changes).click()

    def upperdetails_hotspotlabel(self):
        self.driver.find_element_by_xpath("//div[@class='o-form-field__contents']/div/div/div[2]/div/p").send_keys("New hotspot label")
        self.driver.find_element_by_xpath("//div[@class='c-file__dropzone c-file -dropzone']/input").send_keys(os.path.dirname(os.getcwd())+"\\resources\\file_example_JPG_100kB_assesment_edited.jpg")
        self.driver.find_element_by_xpath("//button[text()='Upload']").click()
        element = self.driver.find_element_by_xpath("//div[@class='c-settings-hotspot__image']")
        ActionChains(self.driver).move_to_element_with_offset(element,30,30).click().perform()
        self.driver.find_element_by_xpath("//a[text()='Hotspot 1 Suggested Answer']/preceding-sibling::button").click()
        self.driver.find_element_by_xpath("//label[text()='Hotspot Answer']/parent::div/div/div/input").send_keys("Text1")
        self.driver.find_element_by_xpath("//i[@class='c-icon -check-thin -status-success -save-icon']").click()
        ActionChains(self.driver).move_to_element_with_offset(element, 40, 40).click().perform()
        self.driver.find_element_by_xpath("//a[text()='Hotspot 2 Suggested Answer']/preceding-sibling::button").click()
        self.driver.find_element_by_xpath("//label[text()='Hotspot Answer']/parent::div/div/div/input").send_keys("Text2")
        self.driver.find_element_by_xpath("//i[@class='c-icon -check-thin -status-success -save-icon']").click()


    def add_hint(self):
        element = self.driver.find_element_by_xpath("//div[@class='c-pullout__section JIT-hint__settings']//div").click()
        self.driver.find_element_by_xpath("//span[text()='Add a Hint']/parent::button").click()
        self.driver.find_element_by_xpath("//div[@class='JIT-hint__section']/div/div/div/div[1]/div/div/div[2]/div/div/div/div/div[2]/div/p").send_keys("Hint1")
        self.driver.find_element_by_xpath("//div[@class='JIT-hint__section']/div/div/div/div[2]/div/a/i[@class='c-icon -check']").click()
        try:

            self.driver.find_element_by_xpath("//span[text()='Add a Hint']/parent::button")
            flag = True
        except NoSuchElementException:
            flag = False
            if flag == False:
                self.assertFalse("adding hint button is not available")
            else:
                print("adding hint button is available")

    def add_hint(self):
        element = self.driver.find_element_by_xpath("//div[@class='c-pullout__section JIT-hint__settings']//div[2]")
        element.click()
        time.sleep(5)
        self.driver.find_element_by_xpath("//span[text()='Add a Hint']/parent::button").click()
        time.sleep(5)
        self.driver.find_element_by_xpath(
            "//div[@class='JIT-hint__section']/div/div/div/div[1]/div/div/div[2]/div/div/div/div/div[2]/div/p").send_keys(
            "Hint1")
        time.sleep(5)
        self.driver.find_element_by_xpath(
            "//div[@class='JIT-hint__section']/div/div/div/div[2]/div/a/i[@class='c-icon -check']").click()
        try:
            self.driver.find_element_by_xpath("//span[text()='Add a Hint']/parent::button")
            flag = "true"
        except NoSuchElementException:
            flag = "false"

        assert flag == "true"
        self.driver.find_element_by_xpath("//span[text()='Add a Hint']/parent::button").click()
        time.sleep(5)
        self.driver.find_element_by_xpath(
            "//div[@class='JIT-hint__section']/div/div/div/div[1]/div/div/div[2]/div/div/div/div/div[2]/div/p").send_keys(
            "Hint2")
        time.sleep(5)
        self.driver.find_element_by_xpath(
            "//div[@class='JIT-hint__section']/div/div/div/div[2]/div/a/i[@class='c-icon -check']").click()
        try:
            self.driver.find_element_by_xpath("//span[text()='Add a Hint']/parent::button")
            flag = "true"
        except NoSuchElementException:
            flag = "false"

        assert flag == "false"

    def validate_hint_settings(self):
        toggelbtntxt = self.driver.find_element_by_xpath("//div[@class='c-pullout__section JIT-hint__settings']//label").text
        toggelbtn = self.driver.find_element_by_xpath("//div[@class='c-pullout__section JIT-hint__settings']//div[2]")
        if toggelbtntxt=="Hints Off":
            time.sleep(5)
            toggelbtn.click()

            if self.driver.find_element_by_xpath("//span[text()='Add a Hint']/parent::button"):
                assert True
            else:
                assert False
        else:
            time.sleep(5)
            toggelbtn.click()
            time.sleep(3)
            toggelbtn.click()

    expectedstring = "Hint1"
    def validate_first_hint(self):
        self.driver.find_element_by_xpath("//*[@class='c-pullout__footer__actions']//button[2]").click()
        time.sleep(10)
        expected = "Please add atleast one hint."

        actual = self.driver.find_element_by_xpath("//div[@class='c-pullout__section JIT-hint__settings']//div[@class='c-hint -warning']/span").text
        if actual == expected:
            print("true")
        else:
            assert False
        self.driver.find_element_by_xpath("//span[text()='Add a Hint']/parent::button").click()
        time.sleep(5)
        self.driver.find_element_by_xpath("//div[@class='JIT-hint__section']/div/div/div/div[1]/div/div/div[2]/div/div/div/div/div[2]/div/p").send_keys(self.expectedstring)
        time.sleep(5)
        self.driver.find_element_by_xpath("//div[@class='JIT-hint__section']/div/div/div/div[2]/div/a/i[@class='c-icon -check']").click()
        time.sleep(5)
        self.driver.find_element_by_xpath("//*[@class='c-pullout__footer__actions']//button[2]").click()
        time.sleep(10)

    def validate_hintsettings_with_one_empty_hint(self):
        element = self.driver.find_element_by_xpath("//div[@class='c-pullout__section JIT-hint__settings']//div[2]")
        element.click()
        time.sleep(5)
        self.driver.find_element_by_xpath("//span[text()='Add a Hint']/parent::button").click()
        time.sleep(5)
        expected= self.driver.find_element_by_xpath("//div[@class='JIT-hint__section']/div/div/div/div[1]/div/div/div[2]/div/div/div/div/div[2]/div/p").send_keys("")
        self.driver.find_element_by_xpath("//div[@class='JIT-hint__section']/div/div/div/div[2]/div/a/i[@class='c-icon -check']").click()
        time.sleep(5)
        self.driver.find_element_by_xpath("//*[@class='c-pullout__footer__actions']//button[2]").click()

        actual =self.driver.find_element_by_xpath("//div[@class='c-pullout__section JIT-hint__settings']//div[@class='c-hint -warning']/span").text
        if actual != expected:
            assert ("Please fill the hint.", actual)

    def copy_component(self,compname):
        self.driver.find_element_by_xpath(
            "//label[text()='" + compname + "']/parent::div[@class='c-icon-label']/parent::div/parent::div/div[2]/div/a/i").click()
        time.sleep(5)
        self.driver.find_element_by_xpath("//a[text()='Copy Component ID']").click()
        time.sleep(5)
        self.driver.find_element_by_xpath("//div[@class='c-toast__body']/p").text


    expectedstring = "Hint1"
    expectedfileupload = "New file upload"
    videodata = "New video answer"
    hotspotstring = "New hotspot label"
    discussiontopic = "New discussion answer Topic"
    discussionbody = "New discussion answer body"
    audiodata = "New audio answer"
    def validate_copy_component(self,compname):

        builder = ActionChains(self.driver)
        descPaste =self.driver.find_element_by_xpath("//input[@placeholder='Enter ID']")
        descPaste.click()
        builder.keyDown(Keys.CONTROL).sendKeys("v").keyUp(Keys.CONTROL).build().perform()
        self.driver.find_element_by_xpath("//button[text()='Apply']").click()
        time.sleep(5)
        self.driver.find_element_by_xpath("//label[text()='" + compname + "']/parent::div[@class='c-icon-label']/parent::div/parent::div/a/i[@class='c-icon -gear']").click()
        time.sleep(5)
        copiedcomponent =self.driver.find_element_by_xpath("//div[@class='JIT-hint__add-hint']/div/div/div[2]/div/div/div/p").text
        print(copiedcomponent)
        if compname=="file upload":
            fileuploadquestion =self.driver.find_element_by_xpath("//h6[text()='Question Text']/parent::div/div/div/div[2]/div/p").text
            if (self.expectedstring == copiedcomponent) and (self.expectedfileupload== fileuploadquestion):
                print("The hint associated with the component is successfully imported on usuing component id")
            else:
                assert False("The hint associated with the component is not successfully imported on usuing component id")
                print("The hint associated with the component is not successfully imported on usuing component id")

        if compname=="Discussion Answer":
            discussbody =self.driver.find_element_by_xpath("//section[@class='c-wrapper']/div[2]/div/div/div/div[2]/div/p").text
            discusstopic =self.driver.find_element_by_xpath("//label[text()='Discussion Topic']/following-sibling::div/div/input").text
            print(discussbody)
            print(discusstopic)
            if (self.expectedstring ==copiedcomponent) and (self.discussionbody == discussbody):
                print("The hint associated with the component is successfully imported on usuing component id")
            else:

                assert False("The hint associated with the component is not successfully imported on usuing component id")
                print("The hint associated with the component is not successfully imported on usuing component id")

        if compname =="Hotspot Label":

            actualstring =self.driver.find_element_by_xpath("//h6[text()='Question Text']/parent::div/div/div/div[2]/div/p").text
            if (actualstring== self.hotspotstring) and (self.expectedstring== copiedcomponent):

                print("The mandatory fields associated with the component is successfully imported on usuing component id")
            else:
                assert False("The mandatory fields associated with the component is not successfully imported on usuing component id")
                print("The mandatory fields associated with the component is not successfully imported on usuing component id")

        if compname == "Video Answer":

            actualstring =self.driver.find_element_by_xpath("//h6[text()='Question Text']/parent::div/div/div/div[2]/div/p").text
            if (actualstring== self.videodata) and (self.expectedstring==copiedcomponent):
                print("The mandatory fields associated with the component is successfully imported on usuing component id")
            else:

                assert False("The mandatory fields associated with the component is not successfully imported on usuing component id")
                print("The mandatory fields associated with the component is not successfully imported on usuing component id")

        if (compname=="Audio Answer"):

            actualstring =self.driver.find_element_by_xpath("//h6[text()='Question Text']/parent::div/div/div/div[2]/div/p").text
            if (actualstring== self.audiodata) and (self.expectedstring== copiedcomponent):
                print("The mandatory fields associated with the component is successfully imported on usuing component id")
            else:
                assert False("The mandatory fields associated with the component is not successfully imported on usuing component id")
                print("The mandatory fields associated with the component is not successfully imported on usuing component id")



