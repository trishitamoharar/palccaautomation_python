import os
import time

from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys

from config.BaseClass import BaseClass


class Learning_Template_Outline_JIT_AlertsPage(BaseClass):

    def __init__(self, driver):
        self.driver = driver
        self.progress = "//div[@class='c-course-sidebar__menu']/div/div[2]/a"
        self.btn_continue = "//*[@class='c-editor-pagebreak__nav__buttons']//button[2]/span"
        self.msg_alert = "//div[@class='Toastify__toast-container Toastify__toast-container--bottom-right jit-alert-wrapper']//div[@class='jit-alert']"
        self.pagetitle2nd = "//li[@class='c-editor-pagebreak__nav__pages-item'][2]/a"

    def clk_Progress(self):
        return self.driver.find_element_by_xpath(self.progress).click()

    def clk_continue(self):
        return self.driver.find_element_by_xpath(self.btn_continue).click()

    def alert_massage(self):
        return self.driver.find_element_by_xpath(self.msg_alert).text

    def pageTitle(self):
        return self.driver.find_element_by_xpath(self.pagetitle2nd).text

    # def First_click_on_continue_and_Validate_JIT_Alert_massage(self):
    #     log = self.getLogger()
    #     ele_alert_text = self.driver.find_element_by_xpath(
    #         "//div[@class='Toastify__toast-container Toastify__toast-container--bottom-right jit-alert-wrapper']//div[@class='jit-alert']").text
    #     log.info(ele_alert_text)
    #     # assert("Hey! It looks like you missed a question. Give it a try.",ele_alert_text)
    #     if "It looks like you missed a question." in ele_alert_text:
    #         log.info("First click on continue and Validate JIT Alert massage .")
    #     else:
    #         log.info("First click on continue and Not Validated JIT Alert massage .")
    #
    def Second_click_on_continue_Page_has_been_navigated_to_2nd_page(self):
        log = self.getLogger()
        pagination = self.driver.find_element_by_xpath("//li[@class='c-editor-pagebreak__nav__pages-item'][2]/a")
        paginationText2nd = pagination.get_attribute("title")
        log.info(paginationText2nd)
        if "Page 2" in paginationText2nd:
            assert True
            log.info("Second click on continue and page has been navigated to 2nd page.")
