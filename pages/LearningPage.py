from selenium.webdriver import ActionChains
from selenium.webdriver.common.keys import Keys

from data.TD_JIT_Hint_Teacher import TD_JIT_Hint_Teacher


class LearningPage:
    def __init__(self, driver):
        self.driver = driver
        self.activecoursestab= "#application > div > article > section > ul > li:nth-child(1) >a"
        self.searchactivecoursesearchbox ="input[placeholder^='Search by course name,']"

    def clk_ActiveCoursesTab(self):
        return self.driver.find_element_by_css_selector(self.activecoursestab).click()

    def SearchActiveCourseSearchbox(self,searchtext):
        self.driver.find_element_by_css_selector(self.searchactivecoursesearchbox).send_keys(searchtext)
        actions = ActionChains(self.driver)
        actions.send_keys(Keys.ENTER)
